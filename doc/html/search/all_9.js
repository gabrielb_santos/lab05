var searchData=
[
  ['scoreboard',['scoreboard',['../round_8h.html#a4c0568b6b6b213c2334e9798b4781335',1,'round.cpp']]],
  ['set_5fdices',['set_dices',['../classPlayer.html#ae5d0e00cb9d7e7062cb9bd657ce9c788',1,'Player']]],
  ['set_5fid',['set_id',['../classPlayer.html#a8fafe42050cd9c29e2667bf234812394',1,'Player']]],
  ['set_5fn',['set_n',['../classPlayer.html#afacedad5837fbbd50330c1c0aa78be9d',1,'Player']]],
  ['set_5fscore',['set_score',['../classPlayer.html#abaddb087459b1d953f4f70ca0934fc06',1,'Player']]],
  ['set_5fstatus',['set_status',['../classPlayer.html#abf7dd2a22ed7f8f2221028c8aea36500',1,'Player']]],
  ['set_5fvictories',['set_victories',['../classPlayer.html#a066e272c03740841656569f1ecd2b45d',1,'Player']]],
  ['show_5fdices',['show_dices',['../round_8h.html#ac86d540d9c881429f882563a8624ac7c',1,'round.cpp']]]
];
