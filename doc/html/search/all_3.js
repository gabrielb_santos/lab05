var searchData=
[
  ['get_5fdice',['get_dice',['../classPlayer.html#a69fb9b7ed4f9b08016ea3e1ceb0cc62a',1,'Player']]],
  ['get_5fid',['get_id',['../classPlayer.html#af9425dfdf1386ec5f49264177caf9cc3',1,'Player']]],
  ['get_5fn',['get_n',['../classPlayer.html#aa7c9d95b4979d0090a3709eadd17e640',1,'Player']]],
  ['get_5fscore',['get_score',['../classPlayer.html#ad65c379a083e7c6656721616f8784059',1,'Player']]],
  ['get_5fstatus',['get_status',['../classPlayer.html#ab217598d8d36d5dfc503a91856338647',1,'Player']]],
  ['get_5fvictories',['get_victories',['../classPlayer.html#a60eeceaa5e8f26b1989e148ec6466239',1,'Player']]],
  ['give_5fup',['give_up',['../round_8h.html#a4d4dd324c981e0a427f0c4ed4375967d',1,'round.cpp']]]
];
